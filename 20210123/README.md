# SPAハンズオン

## 参加条件

- HTML、CSS、JavaScriptの基本文法が分かり、書籍のサンプルコードを理解でき、動かすことができること。

## 環境準備

### Node.jsのインストール

フロントエンドの開発ではCreate React AppやTypeScriptを使用するため、Node.jsをインストールします。
Node.jsのバージョンには、LTSである 12 を使用します。

[公式サイト](https://nodejs.org/ja/)の案内に沿ってインストールしてください。

### Visual Studio Codeのインストール

開発時に使用するエディタをインストールします。
エディタは使い慣れたものなら何でもよいですが、何もインストールしていなければ、
今回のハンズオンで使用するコードに対応できるVisual Studio Codeをインストールします。

[公式サイト](https://azure.microsoft.com/ja-jp/products/visual-studio-code/)の案内に沿ってインストールしてください。

### Docker(Docker Compose)のインストール

開発時にコンテナを使用するため、DockerとDocker Composeをインストールします。

公式サイトの案内に沿って、使用しているOSに対応するDockerとDocker Composeをインストールしてください。
Docker Engineは18.06.0以上をインストールしてください。

WindowsとMacでのDocker利用方法はいくつかありますが、本ハンズオンではDocker Desktopをインストールしてください。
（参考：[Install Docker Desktop on Windows](https://docs.docker.com/docker-for-windows/install/)、[Install Docker Desktop on Mac](https://docs.docker.com/docker-for-mac/install/)）

本ハンズオンでは、Dockerコンテナ起動時にローカルディレクトリをマウントします。
Docker Desktop for WindowsやDocker Desktop for Macでローカルディレクトリをマウントするためには
事前にファイル共有を許可しておく必要があります。
Docker DesktopのSettings→Resources→FILE SHARINGから、本ハンズオンのプロジェクトを配置するドライブやディレクトリを共有可能に設定しておいてください。
（参考：[Docker Desktop for Windows user manual](https://docs.docker.com/docker-for-windows/)、[Docker Desktop for Mac user manual](https://docs.docker.com/docker-for-mac/)）

### Docker(Docker Compose) の動作確認

以下の手順で確認します。  
Windowsを前提として手順を記載しておりますので、他のOSの方は適宜読み替えてください。

- 勉強会当日に使用する作業用フォルダを作成します。 ここでは `c:\work` を作成したとします。
- 以下の内容で、 `c:\work\docker-compose.yml` ファイルを作成します。  
  (書式の都合上versionの前に空白文字が存在するようにみえますが無視してください。)
```yaml
version: '3.7'

services:
  mounttest:
    image: ubuntu:18.04
    volumes:
      - ../work/testdir:/testdir
    command: cat /testdir/testfile.txt
```
- 以下の内容で、 `c:\work\testdir\testfile.txt` ファイルを作成します。  
```text
Mount is successful.
```
- 以下のコマンドを実行します。
```bash
$ cd C:\work\
$ docker-compose up 
```
実行結果に以下のように `Mount is successful.` という文字列が含まれていることを確認してください。
```text
Creating network "mounttest_default" with the default driver
Pulling mounttest (ubuntu:18.04)...
18.04: Pulling from library/ubuntu
171857c49d0f: Pull complete
419640447d26: Pull complete
61e52f862619: Pull complete
Digest: sha256:646942475da61b4ce9cc5b3fadb42642ea90e5d0de46111458e100ff2c7031e6
Status: Downloaded newer image for ubuntu:18.04
Creating mounttest_mounttest_1 ... done
Attaching to mounttest_mounttest_1
mounttest_1  | Mount is successful. ※このように「Mount is successful.」が含まれるログが出力されたら成功
mounttest_mounttest_1 exited with code 0
```
- 以下のコマンドを実行し、動作確認に使用したコンテナを削除してください。
```bash
$ docker-compose down 
```

## 事前学習

JavaScriptとTypeScriptの基礎について学習しておいてください。  
開発に使用するTypeScriptはJavaScriptを拡張したものであるため、JavaScript → TypeScriptの順に学んで頂くと良いと思います。
  - [JavaScript Primer](https://jsprimer.net)  
    以下を読んでください。暗記する必要はありません。参照しながらプログラムを書くことが出来れば良いです。
    - 第一部: 基本文法
      - JavaScriptとは
      - コメント
      - 変数と宣言
      - 値の評価と表示
      - データ型とリテラル
      - 演算子
      - 関数と宣言
      - 文と式
      - 条件分岐
      - ループと反復処理
      - オブジェクト
      - 配列
      - 文字列
  - [TypeScript の概要](https://qiita.com/EBIHARA_kenji/items/4de2a1ee6e2a541246f6)

Reactの公式サイトにあるチュートリアルやガイドを見ておいてください。
  - [Reactを学ぶ](https://ja.reactjs.org/docs/getting-started.html#learn-react)  
    上記のサイトはボリュームが結構ありますが、特に以下の部分を見ておいておいてください。他の部分は余力があればで結構です。
     - [チュートリアル](https://ja.reactjs.org/tutorial/tutorial.html)